# Notes

- **Used React Create App starter kit**

- **Used inline styles**
	I love when js and styles are in same files. But i would use React styled-components for real world project.  

- **Functional components mostly**
	Because of simple state, only top-level component is and actual class with lifecycle hooks and state management. All other components are dump and just work with props.  

- **Events overlapping was the hardest part**
	I did overall structure and basic layout pretty quick for basic no-overlapping events case. But then stuck on overlapping part. I tried to make it proper way - so more events for more users could overlap, and spend more time than i could on that. So after all current implementation handle exactly two events overlapping.  

- **Didn't use latest syntax for clarity**
	I don't know your styleguides, so i did't use some latest syntax sugar.  i bind callback functions in constructor method for clarity, but would rather use class shorthand properties and fat arrows for `this` binding, like so:

	```
	class Event extends Component {
	  handleClick = () => {
	      return true;
	  }
	}
	```

	Also, would use class properties to set initial state:

	```
	class Event extends Component {
	  state = {
	    active: true,
	    isFetched: false
	  }
	}
	```

	It's experimental syntax, but i think safe to use and more cleaner way. Also, there is no chains of async tasks in this project, so i didn't use `async/await`.
