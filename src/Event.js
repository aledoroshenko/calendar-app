import React from 'react';
import d from 'date-fns';

const Events = ({ start, color = '#ccc', end, date }) => {
    const shift = d.differenceInMinutes(start, d.addHours(date, 9));
    const height = d.differenceInMinutes(end, start);

    return (
        <div style={{
            position: 'absolute',
            width: '100%',
            backgroundColor: color,
            top: shift,
            left: 0,
            height: height,
            fontSize: '11px',
            padding: 5
        }}>
            {d.format(start, 'HH:mm')}
        </div>
    )
};

export default Events;