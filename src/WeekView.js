import React, {Component} from 'react';
import _ from 'lodash/fp';
import d from 'date-fns';
import './App.css';
import DayColumn from "./DayColumn";
import WeekHeader from "./WeekHeader";
import TimeGutterColumn from "./TimeGutterColumn";

const isEventOnThisDate = (date) => ({ start }) => {
    return d.isWithinRange(start, d.startOfDay(date), d.endOfDay(date));
};

const WeekView = ({ events, range, users }) => {
    const getDayColumn = date => <DayColumn date={date}
                                            key={_.uniqueId('dayColumn')}
                                            events={_.filter(isEventOnThisDate(date), events)}
                                            users={users}/>;

    const DayColumns = _.map(getDayColumn, range);

    return (
        <div>
            <WeekHeader range={range}/>

            <div style={{ display: 'flex' }}>
                <TimeGutterColumn />
                {DayColumns}
            </div>
        </div>
    )
};


export default WeekView;