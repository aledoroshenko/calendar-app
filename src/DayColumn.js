import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash/fp';
import Timeslots from "./Timeslots";
import EventsWrapper from './EventsWrapper';

const DayColumn = ({ events = [], date, users = [] }) => {
    const hasEventsOnDay = !_.isEmpty(events);

    return (
        <div style={{ width: '20%', borderRight: '1px solid #eee', position: 'relative' }}>
            <Timeslots />
            {hasEventsOnDay && <EventsWrapper {...{ date, users, events }}/>}
        </div>
    )
};

DayColumn.propTypes = {
    events: PropTypes.array.isRequired,
    date: PropTypes.object.isRequired,
    users: PropTypes.array.isRequired
};

DayColumn.defaultProps = {
    users: []
};

export default DayColumn;