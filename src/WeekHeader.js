import React from 'react';
import _ from 'lodash/fp';
import {format} from 'date-fns';

const WeekHeader = ({ range }) =>
    <div style={{ display: 'flex', borderBottom: '1px solid #eee' }}>
        <div style={{ width: 30, flex: 'none' }}/>

        {_.map(date => <div key={_.uniqueId('day')}
                            style={{ width: '20%', marginBottom: 5, fontSize: '12px' }}>
            {format(date, 'DD MMM')}
        </div>, range)}
    </div>;

export default WeekHeader;