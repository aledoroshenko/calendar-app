import React from 'react';
import _ from 'lodash/fp';
import {addHours, setHours} from 'date-fns';

const Timeslots = () => {
    const timeRange = _.times(time => {
        return addHours(setHours(new Date(), 9), time)
    }, 13);

    const slots = _.map(() => {
        return (<div style={{ height: 60, borderBottom: '1px solid #eee' }} key={_.uniqueId('sdf')}/>)
    }, timeRange);

    return (
        <div>{slots}</div>
    )
};

export default Timeslots;