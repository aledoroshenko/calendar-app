import React from 'react';
import _ from 'lodash/fp';
import PropTypes from 'prop-types';
import UserAvatar from "./UserAvatar";

const EventsUsers = ({ users = [] }) => {
    return (
        <div style={{ display: 'flex' }}>
            {_.map(
                ({ photo, name, color }) =>
                    <UserAvatar key={_.uniqueId('user')} {...{ photo, name, color }} />,
                users)}
        </div>
    )
};

EventsUsers.propTypes = {
    users: PropTypes.array.isRequired
};

EventsUsers.defaultProps = {
    users: []
};

export default EventsUsers;