import React from 'react';
import _ from 'lodash/fp';
import d from 'date-fns';
import Event from "./Event";
import EventsGroup from "./EventsGroup";

const EventsWrapper = ({ events, users, date }) => {
    const overlappingEvents = [];

    for (let i = 0; i < events.length; i++) {
        const currentEvent = events[i];

        const overlapping = _.remove(event => {
            return !d.areRangesOverlapping(currentEvent.start, currentEvent.end, event.start, event.end);
        }, events);

        events.splice(i, 1);

        overlappingEvents.push(overlapping);
    }

    const EventElements = _.map(event => {
        const { start, end, name } = event[0];
        const color = _.find(user => user.name === name, users).color;

        if (_.size(event) === 1) {
            return <Event key={_.uniqueId('event')}
                          {...{ start, end, color, name, date }}/>
        }

        return <EventsGroup key={_.uniqueId('eventGroup')}
                            events={event}
                            {...{ users, date }}/>
    }, overlappingEvents);

    return (<div>{EventElements}</div>)
};

export default EventsWrapper;