import d from 'date-fns';
import _ from 'lodash/fp';

const events = [
    {
        "name": "Jeff",
        "events": [
            { "id": 1, "year": "2016", "day": 364, "start": 60, "end": 120 },
            { "id": 2, "year": "2017", "day": 0, "start": 180, "end": 240 },
            { "id": 3, "year": "2017", "day": 0, "start": 360, "end": 420 },
            { "id": 6, "year": "2017", "day": 5, "start": 180, "end": 240 }
        ]
    },
    {
        "name": "Lucy",
        "events": [
            { "id": 4, "year": "2017", "day": 0, "start": 0, "end": 60 },
            { "id": 5, "year": "2017", "day": 0, "start": 180, "end": 300 },
            { "id": 6, "year": "2017", "day": 0, "start": 360, "end": 375 },
            { "id": 6, "year": "2017", "day": 2, "start": 60, "end": 375 }
        ]
    }
];

const allUsers = {
    "Jeff": {
        "name": "Jeff",
        "color": "#ffdc26",
        "photo": "jeff.jpg"
    },
    "Lucy": {
        "name": "Lucy",
        "color": "#8af989",
        "photo": "lucy.jpg"
    },
    "noname": {
        "name": "noname",
        "color": "black",
        "photo": "no-photo.jpg"
    }
};


function getEventsFromServer() {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(events);
        }, 4000);
    });
}

function updateEvents(events) {
    return _.flatMap(({ events, name, id }) => {
        return _.map(({ year, day, start, end, id }) => {
            const fromDate = new Date(year, 0, 1, 9);
            const newStart = d.addMinutes(d.addDays(fromDate, day), start);
            const newEnd = d.addMinutes(d.addDays(fromDate, day), end);

            return { start: newStart, end: newEnd, name, id };
        }, events);
    }, events);
}

export function getUsersData(users) {
    const activeUsers = _.filter(_.flow(_.get('name'), _.includes(_, users)), allUsers);
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(activeUsers);
        }, 2000);
    });
}

export default function getEvents() {
    return getEventsFromServer()
        .then(result => {
            return updateEvents(result);
        })
        .catch(error => {
            console.log('error here');
        })
}