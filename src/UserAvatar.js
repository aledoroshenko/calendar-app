import React from 'react';
import PropTypes from 'prop-types';

const UserAvatar = ({ name = 'Unknown', photo, color = 'red' }) => {
    const border = 4;
    const size = 40;
    const src = require('./' + photo);

    return (
        <div style={{
            textAlign: 'center',
            fontSize: '12px',
            marginLeft: 20,
            width: border * 2 + size,
            height: border * 2 + size,
            borderRadius: '50%',
            borderStyle: 'solid',
            borderWidth: border,
            borderColor: color
        }}>
            <img title={name}
                 style={{ marginBottom: 4, width: size, height: size, borderRadius: '50%' }}
                 src={src}/>
            <span>{name}</span>
        </div>
    )
};

UserAvatar.propTypes = {
    name: PropTypes.string.isRequired,
    photo: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired
};

export default UserAvatar;