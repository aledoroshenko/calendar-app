import d from 'date-fns';

export function getWeekRangeForDate(date) {
    const startOfWeek = d.startOfWeek(date, { weekStartsOn: 1 });
    const endOfWeek = d.addDays(startOfWeek, 6);

    return d.eachDay(startOfWeek, endOfWeek);
}