import React, {Component} from 'react';
import {format} from 'date-fns';
import PropTypes from 'prop-types';

const Toolbar = ({onPrevClick, range, onNextClick}) => {
    return (
        <div style={{ marginLeft: 50, marginTop: -5, display: 'flex' }}>
            <button onClick={onPrevClick}>Prev week</button>
            <div style={{
                marginLeft: 15,
                marginRight: 15,
                fontSize: 14,
                width: 120,
                textAlign: 'center'
            }}>{format(range[0], 'DD MMM')} – {format(range[1], 'DD MMM')}</div>
            <button onClick={onNextClick}>Next week</button>
        </div>
    )
};

Toolbar.propTypes = {
    onPrevClick: PropTypes.func.isRequired,
    onNextClick: PropTypes.func.isRequired,
    range: PropTypes.array.isRequired
};

export default Toolbar;