import React, {Component} from 'react';
import _ from 'lodash/fp';
import d from 'date-fns';
import Spinner from 'react-spinkit';
import WeekView from "./WeekView";
import getEvents, {getUsersData} from './api';
import EventsUsers from "./EventsUsers";
import {getWeekRangeForDate} from "./helpers";
import Toolbar from "./Toolbar";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            events: [],
            isFetching: false,
            date: new Date(2017, 0, 1, 9)
        };

        this.nextWeek = this.nextWeek.bind(this);
        this.prevWeek = this.prevWeek.bind(this);
    }

    nextWeek() {
        this.setState(state => ({
            date: d.addWeeks(state.date, 1)
        }));
    }

    prevWeek() {
        this.setState(state => ({
            date: d.addWeeks(state.date, -1)
        }));
    }

    componentDidMount() {
        this.setState(state => ({ isFetching: true }));

        getEvents()
            .then(events => {
                const users = _.flow(_.map('name'), _.uniq)(events);

                return getUsersData(users)
                    .then(usersData => {
                        this.setState(state => ({ events: events, isFetching: false, users: usersData }));
                    })
            });
    }

    render() {
        const range = getWeekRangeForDate(this.state.date);

        return (
            <div style={{ width: '80%', margin: '0 auto' }}>
                <div style={{
                    marginTop: 30,
                    display: 'flex',
                    justifyContent: 'space-between',
                    height: 60,
                    marginBottom: 50,
                    alignItems: 'center'
                }}>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <div>
                            <h1 style={{ marginTop: 10, marginLeft: 30 }}>
                                Jeff and Lucy use calendar
                            </h1>
                        </div>

                        <Toolbar onNextClick={this.nextWeek}
                                 onPrevClick={this.prevWeek}
                                 range={[_.first(range), _.last(range)]}/>
                    </div>

                    <div>
                        {this.state.isFetching ?
                            <div style={{ marginTop: 10 }}><Spinner spinnerName='three-bounce' noFadeIn/></div> :
                            <EventsUsers users={this.state.users}/>}
                    </div>
                </div>

                <WeekView events={this.state.events}
                          range={range}
                          users={this.state.users}/>
            </div>
        );
    }
}

export default App;
