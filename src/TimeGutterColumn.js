import React from 'react';
import _ from 'lodash/fp';
import {format, addHours, setHours} from 'date-fns';

const TimeGutterColumn = () => {
    const timeRange = _.times(time => {
        return addHours(setHours(new Date(), 9), time)
    }, 13);

    const getCell = time => <div
        style={{ height: 60, borderBottom: '1px solid #eee', borderRight: '1px solid #eee', fontSize: '12px' }}
        key={_.uniqueId('gutter')}>
        <span style={{ top: 10, position: 'relative' }}>{format(time, 'HH')}</span>
    </div>;

    return (<div style={{ width: 30, flex: 'none' }}>
        {_.map(getCell, timeRange)}
    </div>)
};

export default TimeGutterColumn;