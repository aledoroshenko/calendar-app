import React from 'react';
import _ from 'lodash/fp';
import d from 'date-fns';
import './App.css';

const EventsGroup = ({ events, users, date }) => {
    const count = _.size(events);
    let left = -50;
    const Elements = _.map(({ name, start, end }) => {
        const shift = d.differenceInMinutes(start, d.addHours(date, 9));
        const height = d.differenceInMinutes(end, start);
        const color = _.find(user => user.name === name, users).color || '#ccc';
        const width = 100 / count + '%';
        left = left + 50;

        return (
            <div style={{
                position: 'absolute',
                width: width,
                backgroundColor: color,
                left: left + '%',
                top: shift,
                height: height,
                fontSize: '11px',
                padding: 5
            }} key={_.uniqueId('event')}>
                {d.format(start, 'HH:mm')}
            </div>
        )
    }, events);

    return (<div>{Elements}</div>);
};

export default EventsGroup;